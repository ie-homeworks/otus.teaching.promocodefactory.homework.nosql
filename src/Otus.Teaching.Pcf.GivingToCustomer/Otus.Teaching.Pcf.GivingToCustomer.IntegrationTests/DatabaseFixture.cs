﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class DatabaseFixture: IDisposable
    {
        private readonly TestDbInitializer _testDbInitializer;

        public TestDataContext DbContext { get; private set; }
        
        public DatabaseFixture()
        {
            DbContext = new TestDataContext(
                "mongodb://mongo:docker@localhost:5317");
                //"mongodb://mongo:docker@promocode-factory-giving-to-customer-db:27017");

            _testDbInitializer= new TestDbInitializer(DbContext);
            _testDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _testDbInitializer.CleanDb();
        }
    }
}