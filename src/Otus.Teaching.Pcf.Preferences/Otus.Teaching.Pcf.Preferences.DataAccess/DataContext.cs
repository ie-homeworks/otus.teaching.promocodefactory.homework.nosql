﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.Preferences.Core.Domain;

namespace Otus.Teaching.Pcf.Preferences.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {
        }
    }
}
