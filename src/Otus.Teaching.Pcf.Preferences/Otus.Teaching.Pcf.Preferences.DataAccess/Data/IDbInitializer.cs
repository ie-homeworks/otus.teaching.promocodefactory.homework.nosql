﻿namespace Otus.Teaching.Pcf.Preferences.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}