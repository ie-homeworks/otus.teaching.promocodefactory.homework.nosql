﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Middleware
{
    public class ResponseMetricMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<CachingMiddleware> _logger;

        public ResponseMetricMiddleware(RequestDelegate next, ILogger<CachingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var sw = Stopwatch.StartNew();

                await _next(context);
                sw.Stop();
                var elapsed = sw.Elapsed.TotalMilliseconds.ToString();
                _logger.LogInformation($"Request time: {elapsed} ms");
        }
    }

    public static class ResponseMetricExtensions
    {
        public static IApplicationBuilder UseResponseMetric(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ResponseMetricMiddleware>();
        }
    }
}
