﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Middleware
{
    /// <summary>
    /// Http-кэш для get-запросов 
    /// </summary>
    public class CachingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<CachingMiddleware> _logger;

        public CachingMiddleware(RequestDelegate next, ILogger<CachingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, IDistributedCache distributedCahe)
        {
            if (context.Request.Method != "GET")
            {
                await _next(context);
                return;
            }

            context.Request.EnableBuffering();
            var key = $"caching_{context.Request.Path}";
            var cache = await distributedCahe.GetAsync(key);
            if (cache != null)
                await GetDataFromCache(context, distributedCahe, key);
            else
                await SaveDataToCacheEndExecuteNext(context, distributedCahe, _next);
        }

        private async Task GetDataFromCache(HttpContext context,
            IDistributedCache distributedCahe, string key)
        {
            var cache = await distributedCahe.GetAsync(key);
            _logger.LogInformation("taking data from cache");
            context.Response.Headers.Append("Content-Type", "application/json; charset=utf-8");
            context.Response.Headers.Append("Source", "Cache");
            await context.Response.Body.WriteAsync(cache);
        }

        private async Task SaveDataToCacheEndExecuteNext(HttpContext context,
            IDistributedCache distributedCahe, RequestDelegate requestDelegate)
        {
            var responseStream = context.Response.Body;
            var key = $"caching_{context.Request.Path}";
            _logger.LogInformation("taking data from action method");
            await using var ms = new MemoryStream();
            context.Response.Body = ms;
            await requestDelegate(context);
            await distributedCahe.SetAsync(key, ms.ToArray(), new DistributedCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromSeconds(5)
            });
            context.Response.Body = responseStream;
            await context.Response.Body.WriteAsync(ms.ToArray());
        }
    }

    public static class CachingExtensions
    {
        public static IApplicationBuilder UseCaching(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CachingMiddleware>();
        }
    }
}
