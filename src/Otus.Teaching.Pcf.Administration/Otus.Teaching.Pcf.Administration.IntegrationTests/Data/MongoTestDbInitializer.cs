﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class MongoTestDbInitializer
        : IDbInitializer
    {
        private IMongoDatabase _mongoDatabase;

        public MongoTestDbInitializer(MongoUrl mongoUrl)
        {
            var mongoClient = new MongoClient(mongoUrl);
            _mongoDatabase = mongoClient.GetDatabase("AdministrationDb");
        }

        public void InitializeDb()
        {
            CleanDb();
            
            _mongoDatabase.GetCollection<Employee>(nameof(Employee))
                .InsertMany(TestDataFactory.Employees);
            _mongoDatabase.GetCollection<Role>(nameof(Role))
                .InsertMany(TestDataFactory.Roles);
        }

        public void CleanDb()
        {
            _mongoDatabase.DropCollection(nameof(Employee));
            _mongoDatabase.DropCollection(nameof(Role));
        }
    }
}